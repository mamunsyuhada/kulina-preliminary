# Preliminary Test Kulina

[![pipeline status](https://gitlab.com/mamunsyuhada/kulina-preliminary/badges/master/pipeline.svg)](https://gitlab.com/mamunsyuhada/kulina-preliminary/-/tree/master)

[link to repository](https://gitlab.com/mamunsyuhada/kulina-preliminary)

### How to test
-  install package first : ```go install```
-  test all : ```go test -v```
-  test one : ```go test -run=<testFuncTarget>``` ex: ```go test -run=TestKulinaxFood```


## [ 1 ] Kulina x Food
### Expected Result
```go
func TestKulinaxFood(t *testing.T) {
	assert.Equal(t, []string{"1", "2", "Kulina", "4", "Food", "Kulina", "7", "8", "Kulina", "Food", "11", "Kulina", "13", "14", "Kulina x Food"}, kulinaxFood(15))
	assert.Equal(t, []string{"1", "2", "Kulina"}, kulinaxFood(3))
	assert.Equal(t, []string{"1", "2", "Kulina", "4", "Food", "Kulina", "7", "8", "Kulina", "Food"}, kulinaxFood(10))
}
```

### Code
```go
func kulinaxFood(iteration int) []string {
	result := []string{}
	for i := 1; i <= iteration; i++ {
		switch {
		case i%3 == 0 && i%5 == 0:
			result = append(result, "Kulina x Food")
		case i%3 == 0:
			result = append(result, "Kulina")
		case i%5 == 0:
			result = append(result, "Food")
		default:
			result = append(result, strconv.Itoa(i))
		}
	}
	fmt.Println(result)
	return result
}
```
### Run 
```go test -run=TestKulinaxFood```

## [ 3 ] Decode Roman to integer
### Expected Result
```go
func TestFindDissimilarity(t *testing.T) {
	assert.Equal(t, "t", findDissimilarity("", "t"))
	assert.Equal(t, "z", findDissimilarity("bxcn", "bncxz"))
	assert.Equal(t, "a", findDissimilarity("bxcn", "abncx"))
	// assert.Equal(t, "n", findDissimilarity("annqalff", "fqlnannaf"))
}
```
### Code
```go
func findDissimilarity(s string, t string) string {
	sArr := strings.Split(strings.ToLower(s), "")
	tArr := strings.Split(strings.ToLower(t), "")

	tsArr := append(sArr, tArr...)
	if len(tsArr) == 1 {
		return tsArr[0]
	}
	// fmt.Println("tsArr", tsArr)

	var mapping = map[string]int{}

	for _, charOne := range tsArr {
		for _, charTwo := range tsArr {
			if charOne == charTwo {
				mapping[charOne] += 1
			}
		}
	}
	// fmt.Println("mapping", mapping)

	var result string
	for _, valueOne := range mapping {
		for keyTwo, valueTwo := range mapping {
			if valueOne > valueTwo {
				result = keyTwo
			}
		}
	}

	// fmt.Println(value)

	return result
}
```
### Run 
```go test -run=TestFindDissimilarity```
## [ 3 ] Decode Roman to integer
### Expected Result
```go
func TestRomanToInteger(t *testing.T) {
	assert.Equal(t, 0, romanToInteger(""))
	assert.Equal(t, 5, romanToInteger("V"))
	assert.Equal(t, 12, romanToInteger("XII"))
	assert.Equal(t, 2022, romanToInteger("MMXXII"))
	/* condition = hash[arrInput[i]] < hash[arrInput[i+1]] */
	assert.Equal(t, 4, romanToInteger("IV"))
	assert.Equal(t, 9, romanToInteger("IX"))
	assert.Equal(t, 40, romanToInteger("XL"))
	/* can't be decoded return 0*/
	assert.Equal(t, 0, romanToInteger("H"))
	assert.Equal(t, 0, romanToInteger("HH"))
}
```

### Code
```go
func romanToInteger(input string) int {
	input = strings.ToUpper(input)
	arrInput := strings.Split(input, "")
	hash := map[string]int{"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
	inputLength := len(arrInput)
	if inputLength == 0 {
		return 0
	}
	if inputLength == 1 {
		return hash[arrInput[0]]
	}
	sum := hash[arrInput[inputLength-1]]
	for i := inputLength - 2; i >= 0; i-- {
		if hash[arrInput[0]] == 0 {
			return 0
		}
		// fmt.Println(hash[arrInput[i]], hash[arrInput[i+1]]) // 1, 5
		if hash[arrInput[i]] < hash[arrInput[i+1]] {
			sum -= hash[arrInput[i]]
		} else {
			sum += hash[arrInput[i]]
		}
	}
	return sum
}
```
### Run 
```go test -run=TestRomanToInteger```

## Show Coverage

```
go test -coverprofile cover.out
go tool cover -html=cover.out
```