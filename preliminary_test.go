package preliminary

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKulinaxFood(t *testing.T) {
	assert.Equal(t, []string{"1", "2", "Kulina", "4", "Food", "Kulina", "7", "8", "Kulina", "Food", "11", "Kulina", "13", "14", "Kulina x Food"}, kulinaxFood(15))
	assert.Equal(t, []string{"1", "2", "Kulina"}, kulinaxFood(3))
	assert.Equal(t, []string{"1", "2", "Kulina", "4", "Food", "Kulina", "7", "8", "Kulina", "Food"}, kulinaxFood(10))
}

func TestRomanToInteger(t *testing.T) {
	assert.Equal(t, 0, romanToInteger(""))
	assert.Equal(t, 5, romanToInteger("V"))
	assert.Equal(t, 12, romanToInteger("XII"))
	assert.Equal(t, 2022, romanToInteger("MMXXII"))
	/* condition = hash[arrInput[i]] < hash[arrInput[i+1]] */
	assert.Equal(t, 4, romanToInteger("IV"))
	assert.Equal(t, 9, romanToInteger("IX"))
	assert.Equal(t, 40, romanToInteger("XL"))
	/* can't be decoded return 0*/
	assert.Equal(t, 0, romanToInteger("H"))
	assert.Equal(t, 0, romanToInteger("HH"))
}

func TestFindDissimilarity(t *testing.T) {
	assert.Equal(t, "t", findDissimilarity("", "t"))
	assert.Equal(t, "z", findDissimilarity("bxcn", "bncxz"))
	assert.Equal(t, "a", findDissimilarity("bxcn", "abncx"))
	// assert.Equal(t, "n", findDissimilarity("annqalff", "fqlnannaf"))
}
