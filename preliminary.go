package preliminary

import (
	"fmt"
	"strconv"
	"strings"
)

func kulinaxFood(iteration int) []string {
	result := []string{}
	for i := 1; i <= iteration; i++ {
		switch {
		case i%3 == 0 && i%5 == 0:
			result = append(result, "Kulina x Food")
		case i%3 == 0:
			result = append(result, "Kulina")
		case i%5 == 0:
			result = append(result, "Food")
		default:
			result = append(result, strconv.Itoa(i))
		}
	}
	fmt.Println(result)
	return result
}

func romanToInteger(input string) int {
	input = strings.ToUpper(input)
	arrInput := strings.Split(input, "")
	hash := map[string]int{"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
	inputLength := len(arrInput)
	if inputLength == 0 {
		return 0
	}
	if inputLength == 1 {
		return hash[arrInput[0]]
	}
	sum := hash[arrInput[inputLength-1]]
	for i := inputLength - 2; i >= 0; i-- {
		if hash[arrInput[0]] == 0 {
			return 0
		}
		// fmt.Println(hash[arrInput[i]], hash[arrInput[i+1]]) // 1, 5
		if hash[arrInput[i]] < hash[arrInput[i+1]] {
			sum -= hash[arrInput[i]]
		} else {
			sum += hash[arrInput[i]]
		}
	}
	return sum
}

func findDissimilarity(s string, t string) string {
	sArr := strings.Split(strings.ToLower(s), "")
	tArr := strings.Split(strings.ToLower(t), "")

	tsArr := append(sArr, tArr...)
	if len(tsArr) == 1 {
		return tsArr[0]
	}
	// fmt.Println("tsArr", tsArr)

	var mapping = map[string]int{}

	for _, charOne := range tsArr {
		for _, charTwo := range tsArr {
			if charOne == charTwo {
				mapping[charOne] += 1
			}
		}
	}
	// fmt.Println("mapping", mapping)

	var result string
	for _, valueOne := range mapping {
		for keyTwo, valueTwo := range mapping {
			if valueOne > valueTwo {
				result = keyTwo
			}
		}
	}

	// fmt.Println(value)

	return result
}
